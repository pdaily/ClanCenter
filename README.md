# ClanCenter

[![Build Status](https://travis-ci.org/PDaily/ClanCenter.svg)](https://travis-ci.org/PDaily/ClanCenter)

[![Code Climate](https://codeclimate.com/github/PDaily/ClanCenter/badges/gpa.svg)](https://codeclimate.com/github/PDaily/ClanCenter)

[![Coverage Status](https://coveralls.io/repos/PDaily/ClanCenter/badge.svg)](https://coveralls.io/r/PDaily/ClanCenter)
**ClanCenter** is a [Rails 4] app written in [Ruby]. It draws heavy inspiration from [the100.io] in that you can create gaming sessions. Aside from that you can create a database of games and different game modes on the fly. 

### Requirements

* Ruby 2.15

* Rails 4.2.0

* Postgres

### Documentation

Please see the wiki (coming soon!)

### Configuration

* Set up your database in `config/database.yml` 

* Run `rake db:migrate && rake db:seed`

### Testing

* Simply run `rake test`


[the100.io]:https://www.the100.io/
[Rails 4]:http://rubyonrails.org/
[Ruby]:https://www.ruby-lang.org/